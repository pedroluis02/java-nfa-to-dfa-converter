# Converter: from Nondeterministic Finite Automata (NFA) to deterministic Finite Automata (DFA)

NFA (Draw diagram)
---
* Draw states.
* Move states inside canvas.
* Draw transitions.
* Set first state (Right-click inside state).
* Set final states (Right-click inside state).
* Show transitions table.

DFA (Show generated diagram)
---
* Converter from NFA.
* Move estates inside canvas.
* Show conversion details.
* Show transitions table.
