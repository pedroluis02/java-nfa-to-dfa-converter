/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graf;

import at.Aut;
import java.awt.Dimension;
import javax.swing.JOptionPane;
import op.ConversionAFND_AFD;

public class ConGUi extends javax.swing.JFrame {

    private AreaG areaGrafico;
    private AreaG areaAFD;
    private DGTablaTrans dGTablaTrans;
    private ConversionAFND_AFD conversionAFND_AFD;
    private DGDetConversion dGDetConversion;
    
    public ConGUi() {
        initComponents();
        areaGrafico = new AreaG(this);
        areaAFD = new AreaG(this);
        areaAFD.menuConextActivado = false;
        
        jScrollPane1.setViewportView(areaGrafico);
        jScrollPane2.setViewportView(areaAFD);
        
        Dimension d = AreaG.getScreen();
        setSize(d.width - (int)(d.width/2.5), d.height - d.height/4);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        botonNuevoEstado = new javax.swing.JButton();
        botonNuevaTransicion = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        botonTabla = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        botonConvertir = new javax.swing.JButton();
        botonDetalles = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Nuevo"));
        jPanel2.setLayout(new java.awt.GridLayout(2, 1));

        botonNuevoEstado.setText("Estado");
        botonNuevoEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoEstadoActionPerformed(evt);
            }
        });
        jPanel2.add(botonNuevoEstado);

        botonNuevaTransicion.setText("Transición");
        botonNuevaTransicion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevaTransicionActionPerformed(evt);
            }
        });
        jPanel2.add(botonNuevaTransicion);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Diagrama"));
        jPanel4.setLayout(new java.awt.GridLayout(2, 1));

        botonTabla.setText("Tabla");
        botonTabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonTablaActionPerformed(evt);
            }
        });
        jPanel4.add(botonTabla);

        botonNuevo.setText("Nuevo");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });
        jPanel4.add(botonNuevo);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Operaciones"));
        jPanel3.setLayout(new java.awt.GridLayout(2, 1));

        botonConvertir.setText("Convertir a AFD");
        botonConvertir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConvertirActionPerformed(evt);
            }
        });
        jPanel3.add(botonConvertir);

        botonDetalles.setText("Detalles Conversión");
        botonDetalles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDetallesActionPerformed(evt);
            }
        });
        jPanel3.add(botonDetalles);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addGap(57, 57, 57))
        );

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });
        jTabbedPane1.addTab("Diagrama", jScrollPane1);
        jTabbedPane1.addTab("AFD", jScrollPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTabbedPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonNuevoEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoEstadoActionPerformed
        // TODO add your handling code here:
        areaGrafico.click_nuevoEstado();
    }//GEN-LAST:event_botonNuevoEstadoActionPerformed

    private void botonNuevaTransicionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevaTransicionActionPerformed
        // TODO add your handling code here:
        areaGrafico.click_nuevaTransicion();
    }//GEN-LAST:event_botonNuevaTransicionActionPerformed

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        // TODO add your handling code here:
        
        int opt = JOptionPane.showConfirmDialog(this, "Desea crear un nuevo diagrama");
        
        if(opt == JOptionPane.OK_OPTION) {
            areaGrafico.nuevoDiagrama();
            areaAFD.nuevoDiagrama();
            conversionAFND_AFD = null;
        }
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void botonConvertirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConvertirActionPerformed
        // TODO add your handling code here:
        
        Aut af = areaGrafico.getAutomata();
        
        if(af.estados.isEmpty() || af.transiciones.isEmpty() ||
                af.estadoInicial == -1 || af.estadosFinales.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Faltan datos de diagrama");
            return;
        }
        
        conversionAFND_AFD = new ConversionAFND_AFD(af);
        conversionAFND_AFD.conversion();
        areaAFD.setDiagrama(conversionAFND_AFD.diagramaAFD( areaAFD.getPreferredSize() ));
        jTabbedPane1.setSelectedIndex(1);
    }//GEN-LAST:event_botonConvertirActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        // TODO add your handling code here:
        int s = jTabbedPane1.getSelectedIndex();
        if(s == 0) {
            botonNuevoEstado.setEnabled(true);
            botonNuevaTransicion.setEnabled(true);
            botonNuevo.setEnabled(true);
            botonConvertir.setEnabled(true);
            botonDetalles.setEnabled(false);
        } else {
            botonNuevoEstado.setEnabled(false);
            botonNuevaTransicion.setEnabled(false);
            botonNuevo.setEnabled(false);
            botonConvertir.setEnabled(false);
            botonDetalles.setEnabled(true);
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void botonTablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonTablaActionPerformed
        // TODO add your handling code here:
        int s = jTabbedPane1.getSelectedIndex();
        if(s == 0) {
            if(dGTablaTrans == null) {
                dGTablaTrans = new DGTablaTrans(this, areaGrafico.getAutomata(), 1);
            } else {
                 dGTablaTrans.setAutomata(areaGrafico.getAutomata(), 1);
            }
            dGTablaTrans.setTitle("Tabla de Diagrama");
        } else {
            if(conversionAFND_AFD == null) {
                return;
            }
            
            if(dGTablaTrans == null) {
                dGTablaTrans = new DGTablaTrans(this, conversionAFND_AFD.getAFD(), 2);
            } else {
                dGTablaTrans.setAutomata(conversionAFND_AFD.getAFD(), 2);
            }
            dGTablaTrans.setTitle("Tabla de AFD");
        }
        
        dGTablaTrans.setLocationRelativeTo(this);
        dGTablaTrans.show();
    }//GEN-LAST:event_botonTablaActionPerformed

    private void botonDetallesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDetallesActionPerformed
        // TODO add your handling code here:

        if(conversionAFND_AFD == null) {
            return;
        }
        
        int s = jTabbedPane1.getSelectedIndex();
        if(s == 1) {
            if(dGDetConversion == null) {
                dGDetConversion = new DGDetConversion(this, conversionAFND_AFD.getAFD(), 
                        areaGrafico.getAutomata());
                dGDetConversion.setTitle("Detalles de conversión AFND a AFD");
                dGDetConversion.setLocationRelativeTo(this);
            } else {
                dGDetConversion.iniciarDetallesConversion(conversionAFND_AFD.getAFD(), 
                        areaGrafico.getAutomata());
            }
            //dGDetConversion.show();
            dGDetConversion.setVisible(true);
        }
    }//GEN-LAST:event_botonDetallesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConGUi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConGUi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConGUi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConGUi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        ConGUi cgui = new ConGUi();
        cgui.setTitle("Conversor Grafico de AFND a AFD");
        cgui.setLocationRelativeTo(null);
        cgui.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonConvertir;
    private javax.swing.JButton botonDetalles;
    private javax.swing.JButton botonNuevaTransicion;
    private javax.swing.JButton botonNuevo;
    private javax.swing.JButton botonNuevoEstado;
    private javax.swing.JButton botonTabla;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
