
package graf;

import at.Aut;
import at.Est;
import at.Simb;
import at.Trans;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.table.DefaultTableModel;

public class DGTablaTrans extends javax.swing.JDialog {

    private DefaultTableModel model;
    private int tipo;
    
    public DGTablaTrans(java.awt.Frame parent, Aut af, int tipo) {
        super(parent, false);
        initComponents();
        //       
        setAutomata(af, tipo);
    }

    public void setAutomata(Aut af, int tipo) {
        this.tipo = tipo;
        if(tipo == 1 || tipo == 2) {
            iniciarTablaTrans(af);
        }
    }
    
    private void iniciarTablaTrans(Aut af) {
        final int ta = af.alfabeto.size() + 1;
        model = new DefaultTableModel () {
            private boolean editable[] = new boolean[ta];

            @Override
            public boolean isCellEditable(int i, int i1) {
                return editable[i1];
            }
        };
        jTable1.setModel(model);
        //
        crearTabla(af); 
    }

    private void crearTabla(Aut af) {
        Simb []sim = new Simb[af.alfabeto.size() + 1];
        sim[0] = new Simb(1, "Estados/Simbolos");
        System.arraycopy(af.alfabeto.toArray(), 0, sim, 1, af.alfabeto.size());
        model.setColumnIdentifiers(sim);
        
        Iterator<Integer> it = af.estados.keySet().iterator();
        int k = 0;
        while(it.hasNext()) {
            int es_pos = it.next();
            String []fd = new String[af.alfabeto.size() + 1];
            String eafd = tipo== 2 ? es_pos + "" : 
                    af.getEstado( es_pos ).getNombre();
            if(af.estadosFinales.contains( es_pos  )) {
                eafd = "#" + eafd;
            }
            
            if(af.estadoInicial == es_pos) {
                eafd = ">" + eafd;
            }
            
            fd[0] = eafd;
            model.insertRow(k, fd);
            k++;
        }
        //
        it = af.estados.keySet().iterator();
        int i = 0;
        while(it.hasNext()) {
            Est es = af.getEstado( it.next() );
            ArrayList <String> idtrans = es.getIdTransiciones();
            //
            System.out.println("DDD: " + es.getPosId());
            if(!idtrans.isEmpty()) {
                analizaridTrans(af, es.getPosId(), i, idtrans);
            }
            i++;
        }
        //
    }
    
    private void analizaridTrans(Aut af, int estado, int pos, ArrayList<String> idtrans) {
        Iterator <String> it = idtrans.iterator();
        while(it.hasNext()) {
            String aux =  it.next();
            Trans t = af.getTransicion( aux );
            String []es = aux.split("-");
            int es_p = Integer.parseInt( es[0] );
            int es_l = Integer.parseInt( es[1] );
            if(es_p == estado) {
                int j = 0;
                
                while(j < t.getIDSimbolos().size()) {
                    String aux_cont = (String) model.getValueAt(pos, 
                            t.getIDSimbolos().get(j) + 1);
                    String nombre;
                    
                    if(tipo == 2) {
                        nombre = af.getEstado( es_l ).getPosId()+"";
                    } else {
                        nombre = af.getEstado( es_l ).getNombre();
                    }
                    
                    if(aux_cont == null || aux_cont.isEmpty()) {
                        aux_cont = nombre;
                    } else {
                        if(tipo == 2) {
                            aux_cont = nombre;
                        } else {
                           aux_cont += "," + nombre; 
                        }
                        
                    }
                    model.setValueAt(aux_cont, pos, t.getIDSimbolos().get(j) + 1);
                    j++;
                }
            }
            
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
