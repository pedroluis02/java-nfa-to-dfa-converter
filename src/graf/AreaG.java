
package graf;

import at.Aut;
import at.Est;
import at.Simb;
import at.Trans;
import diag.CA;
import diag.Arist_A;
import diag.Diag;
import diag.EstadoGP;
import diag.ArAjust_A;
import diag.LinA;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.event.MouseInputListener;

public class AreaG extends JPanel implements MouseInputListener, 
        ActionListener {
    private JFrame parent;
    // -- diagrama - automata
    private Aut af; // automata
    private Diag di; // diagrama
    // --
    private Point punto, 
            puntoRef; // punto referencia para linea de transicion previa.
    private int izq, der, // puntos de # de estado por boton de raton.
            prioEstado; // priorizacion de un estado por superposion de otro.
    private int numEstado, numSimbolo;
    private int btnActivado; // accion[NUEVO_ESTADO | TRANSICION | NINGUNA]
    // grafico
    private Graphics2D graphics;
    //private Image image;
    
    // dialog
    private JPopupMenu menucontextEs;
    private JMenuItem[] menuitensEs;
    private JCheckBoxMenuItem[] cboxsEs;
    // etiqueta
    JPopupMenu menuContextEti;
    JMenuItem[] menuitensEti;
    
    public boolean menuConextActivado;
        
    public AreaG(final JFrame parent) {
        this.parent = parent;
        //this.parentState = parent.getState();
        af = new Aut();
        di = new Diag();
        this.initComponentsDiag();
        this.initMenuContextualEs();
        this.initMenuContextualEti();
        this.setPreferredSize(getScreen());
        //
        menuConextActivado = true;
    }
    private void initComponentsDiag() {
        this.punto    = new Point(-1, -1);
        this.puntoRef = new Point(-1, -1);
        this.izq = -1;
        this.der = -1;
        this.prioEstado = -1;
        this.numEstado = 0;
        this.numSimbolo = 0;
        this.btnActivado = CA.NONE;
        this.setBackground(CA.canvas.COLOR_BACKGROUND);
        this.setForeground(CA.canvas.COLOR_BACKGROUND);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }
    private void initMenuContextualEs() {
        menucontextEs = new JPopupMenu();
        cboxsEs = new JCheckBoxMenuItem[2];
        cboxsEs[0] = new JCheckBoxMenuItem("Inicial");
        cboxsEs[0].setActionCommand("inicial");
        cboxsEs[0].addActionListener(this);
        
        cboxsEs[1] = new JCheckBoxMenuItem("Final");
        cboxsEs[1].setActionCommand("final");
        cboxsEs[1].addActionListener(this);
        
        menucontextEs.add(cboxsEs[0]); menucontextEs.add(cboxsEs[1]); 
        
        add(menucontextEs);
    }
    private void initMenuContextualEti(){
        menuContextEti = new JPopupMenu();
        menuitensEti = new JMenuItem[1];
        
        menuitensEti[0] = new JMenuItem("Editar");
        menuitensEti[0].setActionCommand("editar");
        menuitensEti[0].addActionListener(this);
        
        menuContextEti.add(menuitensEti[0]);
        
        add(menuContextEti);
    }
    @Override
    public void paint(Graphics g){
        super.paint(g);
        graphics = (Graphics2D)g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        paintAutomata(graphics);
    }
    /* --- graphics --- */
    private void paintAutomata(Graphics2D g){
        
        if(di.estados_gp.isEmpty()) {
            return;
        }
        
        Iterator <EstadoGP> it = di.estados_gp.values().iterator();
        while(it.hasNext()){
            EstadoGP e = it.next();
            if(prioEstado == e.getPosId()){
                continue;
            }
            e.draw(g);
            paintAristas(g, e.getIdAristas());
        }
        if(prioEstado != -1){
            EstadoGP es = di.estados_gp.get(prioEstado);
            es.draw(g);
            paintAristas(g, es.getIdAristas());
        }
        if(puntoRef.x != -1 && btnActivado == CA.active.BTNTRANS){
            punto = getPointTransPrev(di.estados_gp.get(der).getPunto(),
                    puntoRef);
            paintAristaPrevia(g, punto, puntoRef, false);
        }
    }
    private void estadoMovido(Point p, int pos) {   
        EstadoGP es = di.getEstadoGP(pos); 
        //es.clearDraw(g);
        p.setLocation(p.x - CA.estado.RADIO, p.y - CA.estado.RADIO);
        es.setPunto(p);
    }
    private void paintAristaPrevia(Graphics2D g, Point pi, Point pf, 
            boolean clean){
        if(clean){
           g.setColor(CA.canvas.COLOR_BACKGROUND);
        }
        else{
           g.setColor(CA.trans.COLOR_PRE);
        }
        g.drawLine(pi.x, pi.y, pf.x, pf.y);
    }
    private void paintArista(int ei, int ef){ 
               
        String simbolo = 
                JOptionPane.showInputDialog(parent, "Simbolo");
        if(simbolo == null){
            return;
        }

        if(simbolo.isEmpty()){
            return;
        }

        int posS = af.existeSimbolo(simbolo);
        if(posS == -1) {
            af.alfabeto.add(new Simb(numSimbolo, simbolo));
            posS = numSimbolo;
            numSimbolo++;
        }
        
        if(posS != -1){
            if(af.existeTransicion(ei + "-" + ef)){
                Trans t = af.getTransicion(ei + "-" + ef);
                if(t.existeSimbolo(posS)){
                    JOptionPane.showMessageDialog(parent, "Transición con '"
                       + simbolo +"' ya existe.",
                       "Agregar Transición", JOptionPane.WARNING_MESSAGE);
                }             
                else {
                    t.addNuevoSimbolo(posS);
                    Arist_A a = di.getArista(ei + "-" + ef);
                    a.setSimbolos(a.getSimbolos() + "," + simbolo);
                }
            }
            else {
                Trans tn = new Trans(ei, posS, ef);
                af.addTransicion(ei + "-" + ef, tn);
                af.estados.get(ei).addIdTransicion(ei + "-" + ef);

                int tipo = ArAjust_A.LINEA;
                if(ei == ef){
                    tipo = ArAjust_A.ARCO;
                }
                else if((ei != ef) && 
                        di.existeArista(ef + "-" + ei)){
                    tipo = ArAjust_A.LINEA_QUEBRADA;
                    Arist_A aux = di.getArista(ef + "-" + ei);
                    //aux.clearDraw(graphics);
                    aux.setNuevoTipo(ArAjust_A.LINEA_QUEBRADA, di);
                } 

                Arist_A an = new Arist_A(ei, simbolo, ef, tipo, di);
                di.addArista(ei + "-" + ef, an);
                di.estados_gp.get(ei).addIdArista(ei + "-" + ef);

                if(ei != ef) {
                    af.estados.get(ef).addIdTransicion(ei + "-" + ef);
                    di.estados_gp.get(ef).addIdArista(ei + "-" + ef);
                }
            }
        }
        else {
            JOptionPane.showMessageDialog(parent, "Símbolo '"
                       + simbolo +"' no existe.",
                       "Agregar Transición", JOptionPane.WARNING_MESSAGE);
        }
    }
    private void paintAristas(Graphics2D g, ArrayList <String> eat){
        int j = 0; Arist_A ar;
        while(j < eat.size()){
            ar = di.getArista(eat.get(j));
            ar.draw(g);
            j++;
        }
    }
    private void aristasMovidas(int pos){
        ArrayList <String> eat = di.estados_gp.get(pos).getIdAristas();
        int j = 0; Arist_A ar;
        while(j < eat.size()){
            ar = di.getArista(eat.get(j));
            //ar.clearDraw(g);
            if(ar.getEstadoL() == ar.getEstadoP()){
                ar.setPuntos(di.getEstadoGP(pos).getPunto(), 
                    di.getEstadoGP(pos).getPunto());
            }else{
                String bs =  eat.get(j);
                int pos_r = bs.indexOf("-");
                Point2D aux; 
                        
                if(bs.substring(0, pos_r).compareTo(""+pos) == 0){
                    aux = di.getEstadoGP(ar.getEstadoL()).getCentro();
                    ar.setPuntos(di.getEstadoGP(pos).getCentro(), aux); 
                }else{
                    aux = di.getEstadoGP(pos).getCentro();
                    ar.setPuntos(di.getEstadoGP(ar.getEstadoP()).getCentro(), 
                            aux);
                }
            }
            j++;
        }
    } 
    /* --- Events --- */
    @Override
    public void mouseDragged(MouseEvent e) {
            if(btnActivado == CA.active.BTNTRANS) {
                if(der == -1) {
                    btnActivado = CA.NONE;
                    return;
                }
                paintAristaPrevia(graphics, punto, puntoRef, true);
                puntoRef = e.getPoint();
                repaint();
            }
            else {
                if(izq == -1) {
                    btnActivado = CA.NONE;
                    return;
                }
                estadoMovido(e.getPoint(), izq);
                if(!di.estados_gp.get(izq).getIdAristas().isEmpty()) {
                    aristasMovidas(izq);
                }
                repaint();
            }
    }
    @Override
    public void mouseMoved(MouseEvent e){
    }
    @Override
    public void mouseClicked(MouseEvent e){
        if(e.getButton() == MouseEvent.BUTTON1){
            if(btnActivado == CA.active.BTNSTATE) {
                Point p = e.getPoint();
                p.translate(-CA.estado.RADIO, -CA.estado.RADIO);
                af.addEstado(numEstado, new Est(numEstado, "q" + numEstado));
                di.addEstadoGP(numEstado,
                        new EstadoGP(numEstado, p, "q" + numEstado));  
                repaint();
                numEstado++;
                btnActivado = CA.NONE;
            }
        }   
    }
    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1) {
            if(btnActivado == CA.active.BTNTRANS) {
                der = di.idEstadoActual(e.getPoint());
            }
            else if(btnActivado == CA.NONE) {
                izq = di.idEstadoActual(e.getPoint());
                prioEstado = izq;
                if(izq != -1){
                    setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                }
            }
        }
        
        if(e.isPopupTrigger() && menuConextActivado) {
            der = di.idEstadoActual(e.getPoint());
            if(der != -1) {
                EstadoGP es = di.getEstadoGP(der);
                menucontextEs.setLabel("Estado: " + es.getNombre());
                if(af.estadoInicial == -1){
                    cboxsEs[0].setState(false);
                    cboxsEs[0].setEnabled(true);
                }else if(der == af.estadoInicial){
                    cboxsEs[0].setState(true);
                    cboxsEs[0].setEnabled(true);
                }else{
                    cboxsEs[0].setState(false);
                    cboxsEs[0].setEnabled(false);
                }

                cboxsEs[1].setState(es.isFinal());
                menucontextEs.show(this, e.getX(), e.getY());                
                return;
            }
            
            Arist_A ar = di.idAristaActual(e.getPoint());
            if(ar != null) {
                menuContextEti.setLabel("Transición \n"+
                        di.getEstadoGP(ar.getEstadoP()).getNombre() +" ==> "+
                        di.getEstadoGP(ar.getEstadoL()).getNombre());
                menuContextEti.show(this, e.getX(), e.getY());
            }
        }
    }
    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1) {
            if(btnActivado == CA.active.BTNTRANS && der != -1) {
                int ptf = di.idEstadoActual(e.getPoint());
                paintAristaPrevia(graphics, punto, e.getPoint(), true);
                if(ptf != -1) {
                    paintArista(der, ptf);
                }
                repaint();
                btnActivado = CA.NONE;
            } else if(btnActivado == CA.NONE && izq != -1) {
                btnActivado = CA.NONE;
                izq = -1;
            }
            puntoRef.setLocation(-1, -1);
            setCursor(Cursor.getDefaultCursor());
        }
    }
    @Override
    public void mouseEntered(MouseEvent e){}
    @Override
    public void mouseExited(MouseEvent e) {
        /*if(btnActivado == CA.active.BTNSTATE) {
            izq = -1;
            setCursor(Cursor.getDefaultCursor());
            btnActivado = CA.NONE;
        }else*/ if(btnActivado == CA.active.BTNTRANS){
            setCursor(Cursor.getDefaultCursor());
            der = -1;
            btnActivado = CA.NONE;
            paintAristaPrevia(graphics, punto, puntoRef, true);
            repaint();
        }
    }
    /* --- functions 1 --- */
    public void nuevoDiagrama() {
        this.af.nuevo();
        this.di.nuevo();
        this.punto    = new Point(-1, -1);
        this.puntoRef = new Point(-1, -1);
        this.izq = -1;
        this.der = -1;
        this.prioEstado = -1;
        this.numEstado = 0;
        this.numSimbolo = 0;
        this.btnActivado = CA.NONE;
        this.repaint();
    }
    
    public void click_nuevoEstado() {
        btnActivado = CA.active.BTNSTATE;
        setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
    }
    
    public void click_nuevaTransicion() {
        btnActivado = CA.active.BTNTRANS;
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
    public void setDiagrama(Diag di) {
        this.di = di;
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        String accion = ae.getActionCommand();
        if(accion.compareTo("inicial") == 0) {
            EstadoGP es = di.getEstadoGP(der);
            es.setInicial(cboxsEs[0].getState());
            if(cboxsEs[0].getState()) {
                af.estadoInicial = der;
            } else {
                af.estadoInicial = -1;
            }
            der = -1;
            repaint();
        }
        else if(accion.compareTo("final") == 0) {
            if(cboxsEs[1].getState()) {
                di.getEstadoGP(der).setFinal(true);
                af.estadosFinales.add(der);
            } else {
                di.getEstadoGP(der).setFinal(false);
                af.estadosFinales.remove(new Integer(der));
            }
            
            der = -1;
            repaint();
        }
    }
    /* --- Other functions --- */
    public static Dimension getScreen(){
       Toolkit toolkit = Toolkit.getDefaultToolkit();
       return new Dimension(toolkit.getScreenSize());
    }
    private Point getPointTransPrev(Point pi, Point pf){
        Point p = (Point)pi.clone();
        Point pfaux = (Point)pf.clone();
        p.translate(CA.estado.RADIO, CA.estado.RADIO);
        LinA auxL = new LinA(p, pfaux);
        auxL.ajustar();
        return new Point((int)auxL.getX1(), (int)auxL.getY1());
    }    
    //
    public Aut getAutomata() {
        return af;
    }
}