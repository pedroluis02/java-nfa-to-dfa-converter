/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package at;

import java.util.ArrayList;
import java.util.HashMap;

public class Aut
{   
    public HashMap <Integer, Est> estados;
    public ArrayList <Simb> alfabeto;
    public int estadoInicial;
    public ArrayList <Integer> estadosFinales;
    public HashMap <String, Trans> transiciones;
    
    public Aut(){
       this.estados   = new HashMap <> ();
       this.alfabeto  = new ArrayList <> ();
       this.estadoInicial  = -1;
       this.estadosFinales = new ArrayList <> ();
       this.transiciones   = new HashMap <> ();
    }
    public int existeInicial(){
        return estadoInicial;
    }
    public void eliminarEstado(int i){
        if(i >= 0 || i < estados.size()) {
            this.estados.remove(i);
        }
    }
    // buscar simbolo en el alfabeto
    public int existeSimbolo(String s){
        int i = 0; 
        while(i < alfabeto.size()){
            Simb aux = alfabeto.get(i);
            if(s.compareTo(aux.getS()) == 0){
                return aux.getPos();
            }
            i++;
        }
        return -1;
    }
    // estados
    public void addEstado(int k, Est e){
        estados.put(k, e);
    }
    public Est getEstado(int k){
        return estados.get(k);
    }
    public boolean existeEstado(int k){
        return estados.containsKey(k);
    }
    // transiciones
    public void addTransicion(String k, Trans t){
            transiciones.put(k, t);
    }
    public Trans getTransicion(String k){
        return transiciones.get(k);
    }
    public boolean existeTransicion(String k){
        return transiciones.containsKey(k);
    }
    // nuevo automata
    public void nuevo(){
        this.estadoInicial = -1;
        this.estados.clear();
        this.estadosFinales.clear();
        this.alfabeto.clear();
        this.transiciones.clear();
    }
}
