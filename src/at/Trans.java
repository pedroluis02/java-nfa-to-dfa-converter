/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at;

import java.util.ArrayList;

public class Trans {
    private int posId_P; // id estado de partida.
    private ArrayList <Integer> idSimbolos; // ids de simbolos.
    private int posId_L; // id estado de llegada.

    public Trans(int posId_P, int posId_S, int posId_L) {
        this.idSimbolos = new ArrayList <> ();
        this.idSimbolos.add(posId_S);
        
        this.posId_P = posId_P;
        this.posId_L = posId_L;
    }
    public Trans(int posId_P, ArrayList <Integer> idSimbolos,
            int posId_L) {
        this.posId_P = posId_P;
        this.idSimbolos = idSimbolos;
        this.posId_L = posId_L;
    }
    
    public int getPosId_P() {
        return posId_P;
    }
    
    public void addNuevoSimbolo(int idPos_S){
        idSimbolos.add(idPos_S);
    }
    public ArrayList <Integer> getIDSimbolos(){
        return idSimbolos;
    }

    public boolean existeSimbolo(int posS) {
        int i = 0;
        while(i < idSimbolos.size()){
            if(posS == idSimbolos.get(i)){
                return true;
            }
            i++;
        }
        return false;
    }
}
