/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at;

import java.util.ArrayList;

public class Est {
    private int posId;
    private String nombre;
    
    private ArrayList <String> trans; // trans
    
    public Est(int posId, String nombre){
        this.posId = posId;
        this.nombre = nombre;
        this.trans = new ArrayList <> ();
    }
    // get
    public int getPosId(){
        return posId;
    }
    public String getNombre(){
        return nombre;
    }
    // set
    public void setPosId(int posId){
        this.posId = posId;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    // trans
    public void addIdTransicion(String tId){
        trans.add(tId);
    }
    public ArrayList <String> getIdTransiciones(){
        return trans;
    }
}
