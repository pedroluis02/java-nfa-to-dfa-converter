/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at;

public class Simb{
    private int pos;
    private String se;
    
    public Simb(int pos, String se){
        this.pos = pos;
        this.se = se;
    }
    //-----------------------------------
    public String getS(){
        return se;
    }
    public int getPos(){
        return pos;
    }

    @Override
    public String toString() {
        return se;
    }
    
}
