/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diag;

import java.awt.Color;
import java.awt.Font;

public final class CA {
    
    public static final int NONE = 0;
    
    public static final class estado{
        //Ex y Ey
        public static final int EX = 50;
        public static final int EY = 50;
        public static final int RADIO = 25;
        
        //cadena de fuente
        public static final int SIZE_NAME = 11;
        
        public static final Font FONT_S = 
                new Font(Font.SANS_SERIF, Font.BOLD, SIZE_NAME);
        
        //colores
        public static final Color COLOR_BORDER = Color.BLUE;
        public static final Color COLOR_FILL   = Color.YELLOW;
        public static final Color COLOR_FONT   = Color.BLACK;
    }
    
    public static final class canvas{
        //color de fondo del lienzo
        public static final Color COLOR_BACKGROUND = Color.WHITE;
    }
    
    public static final class trans{
        public static final int SIZE_TRANS = estado.RADIO + 15;
        public static final Color COLOR_CURVE = Color.RED;
        public static final Color COLOR_LABEL = Color.BLACK;
        public static final Color COLOR_PRE = Color.GREEN;
        
        public static final double SIZE_FLECHA = 10;
        public static final double SIZE_FM = estado.EX + 20;
        
        public static final double SIZE_MIN = 10;
        
        public static final Font FONT_T = 
                new Font(Font.SANS_SERIF, Font.BOLD, estado.SIZE_NAME + 2);
    }
    
    public static final class geo{
        public static final double PI = 
                3.14159265358979323846264338327950288419717D;
    }
    
    public static final class active{
        public static final int BTNSTATE = 1;
        public static final int BTNTRANS = 2;
    }
}
