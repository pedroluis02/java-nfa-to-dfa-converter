/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diag;

import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

public class ArAjust_A extends Path2D.Double {
    
    public static final int ARCO = 20;
    public static final int LINEA = 22;
    public static final int LINEA_QUEBRADA = 23;
    
    private Arc2D arco;
    private LinA linea;
    private LinA linea_q;
    
    private int tipo;
    private boolean lonA; // la arista es quebrada y los estados se superponen
    public ArAjust_A(Point2D pi, Point2D pf, int tipo) {
        this.tipo = tipo;
        if(tipo == ARCO) {
            arco = new Arc2D.Double(pi.getX(), pi.getY() - 15, 
                  CA.estado.EX, CA.estado.EY, -15, 210, Arc2D.OPEN);
        }
        else if(tipo == LINEA) {
            linea = new LinA(pi, pf);
            linea.ajustar();
            linea.setFlecha(LinA.FLECHA_FINAL);
        }
        else if(tipo == LINEA_QUEBRADA) {
            lonA = true;
            lineaQuebrada(pi, pf);
        }
    }

    private void lineaQuebrada(Point2D pi, Point2D pf){
        
        LinA lprincipal = new LinA(pi, pf);
        lprincipal.ajustar();
        if(lprincipal.longitud() < 1.0) {
            linea = new LinA(pf, pf);
            lonA = false;
            return;
        }

        Point2D puMedio = lprincipal.getPuntoMedio();
        
        double anElevacion = lprincipal.getAngulo();
        // punto medio elevacion
        Point2D puElevacion = new Point2D.Double(puMedio.getX() + 
                Math.sin(anElevacion - CA.geo.PI) * CA.trans.SIZE_TRANS,
                puMedio.getY() + Math.cos(anElevacion - CA.geo.PI) * CA.trans.SIZE_TRANS);
        
        linea_q = new LinA(lprincipal.getP1(), puElevacion);   
        
        /// punto auxiliar medio de elevacion
        Point2D puElevacion_aux = new Point2D.Double(puMedio.getX() + 
                Math.sin(anElevacion - CA.geo.PI) * CA.trans.SIZE_FM, puMedio.getY() + 
                Math.cos(anElevacion - CA.geo.PI) * CA.trans.SIZE_FM);

        LinA laux = new LinA(puElevacion_aux, pf);
        laux.ajustar();
        
        linea = new LinA(puElevacion, laux.getP2());
        linea.setFlecha(LinA.FLECHA_FINAL);
        lonA = true;
    }
    
    public Point2D getPuntoMedio(){
        if(tipo == ARCO){
            return new Point2D.Double(arco.getBounds2D().getX(), 
                    arco.getBounds2D().getY());
        }
        else if(tipo == LINEA){
            return linea.getPuntoMedio();
        }
        else{
            return linea.getP1();
        }
    }
    
    public void draw(Graphics2D g){
        if(tipo == ARCO){
            g.draw(arco);
        }
        else if(tipo == LINEA){
            linea.draw(g);
        }
        else if(tipo == LINEA_QUEBRADA) {
            if(lonA) {
                linea_q.draw(g);
            }
            linea.draw(g);
        }
    }

    public int getTipo() {
        return tipo;
    }
}
