/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package diag;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class EstadoGP {
    private int posId;
    private Ellipse2D circunferencia;
    private String nombre;
    private boolean inicio;
    private boolean fin;
    private boolean seleccionado;
    private ArrayList <String> idAristas; // aristas
    
    public EstadoGP(int posId, Point punto, String nombre) {
        this.posId = posId;
        this.circunferencia = new Ellipse2D.Double(punto.x, punto.y, 
                CA.estado.EX, CA.estado.EX);
        this.nombre = nombre;
        this.inicio = false;
        this.fin   = false;
        this.seleccionado = false;
        this.idAristas = new ArrayList <>();
    }
    
    public int getPosId(){
        return posId;
    }
    public boolean isInicial(){
        return inicio;
    }
    public boolean isFinal(){
        return fin;
    }
    public Point getPunto(){
         return new Point((int)circunferencia.getX(),
                 (int)circunferencia.getY());
    }
    public String getNombre(){
        return nombre;
    }
    public Point2D getCentro(){
        return new Point2D.Double(circunferencia.getCenterX(),
                circunferencia.getCenterY());
    }
    public boolean puntoDentro(double x, double y){
        return circunferencia.contains(x, y);
    }
    //-------------------------------
    public void addIdArista(String idArista){
        idAristas.add(idArista);
    }
    public ArrayList <String> getIdAristas(){
        return idAristas;
    }
    //-------------------------------
    public void setPosId(int posId){
        this.posId = posId;
    }
    
    public void setNmbre(String name){
        this.nombre = name;
    }

    public void setPunto(Point punto){
        circunferencia.setFrame(punto.x, punto.y, 
                CA.estado.EX, CA.estado.EX);
    }

    public void setInicial(boolean inicio){
        this.inicio = inicio;
    }

    public void setFinal(boolean fin){
        this.fin = fin;
    }
    // pintado    
    private void drawDentro(Graphics2D g, Color c){
        g.setColor(c);
        g.fill(circunferencia);
    }
    private void drawBorde(Graphics2D g, Color c){
        g.setColor(c);
        g.draw(circunferencia);
    }
    private void drawInicial(Graphics2D g, Color c){
        g.setColor(c);
        Point punto = new Point((int)circunferencia.getX(),
                (int)circunferencia.getY());
        g.drawLine(punto.x - 15, punto.y + 20, punto.x, punto.y + 30);
        g.drawLine(punto.x - 15, punto.y + 40, punto.x, punto.y + 30);
    }
    
    public void drawInicial(Graphics2D g){
        drawInicial(g, CA.estado.COLOR_BORDER);
    }
    public void drawFinal(Graphics2D g){
        g.setColor(CA.estado.COLOR_BORDER);
        Point punto = new Point((int)circunferencia.getX(),
                (int)circunferencia.getY());
        g.drawOval(punto.x + 3, punto.y + 3, CA.estado.EY - 6, CA.estado.EY - 6);
    }
    
    public void cleaDrawInicial(Graphics2D g){
        drawInicial(g, CA.canvas.COLOR_BACKGROUND);
    }
    public void draw(Graphics2D g){
        drawDentro(g, CA.estado.COLOR_FILL);
        // nombre
        g.setColor(CA.estado.COLOR_FONT);
        g.setFont(CA.estado.FONT_S);
        Point punto = new Point((int)circunferencia.getX(),
                (int)circunferencia.getY());
        g.drawString(nombre, punto.x + (int)(CA.estado.EX/3), 
                punto.y + (int)(CA.estado.EY/2));
        // borde
        drawBorde(g, CA.estado.COLOR_BORDER);
        if(seleccionado) {
            float punteo[] = new float[]{5f, 5f};
            g.setStroke(new BasicStroke(0.8f, 
                    BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER,
                    5f, punteo, 2f));
            g.draw(circunferencia.getBounds2D());
            g.setStroke(new BasicStroke(1));
        }
        if(inicio) {
            drawInicial(g);
        }
        if(fin) {
            drawFinal(g);
        }
    }
    
    public void clearDraw(Graphics2D g){
        Color c = CA.canvas.COLOR_BACKGROUND;
        drawDentro(g, c);
        drawBorde(g, c);
        if(inicio){
            drawInicial(g, c);
        }
    }
}
