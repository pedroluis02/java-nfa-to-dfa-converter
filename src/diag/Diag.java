/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diag;

import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;

public class Diag {
    public HashMap <Integer, EstadoGP> estados_gp;
    public HashMap <String, Arist_A> aristas;
    public int seleccionado;
    
    public Diag() {
        estados_gp = new HashMap <> ();
        aristas = new HashMap <> ();
        seleccionado = -1;
    }

    //
    public void nuevo() {
        estados_gp.clear();
        aristas.clear();
        seleccionado = -1;
    }
    // aritas
    public void addArista(String k, Arist_A a){
        aristas.put(k, a);
    }
    public Arist_A getArista(String k){
        return aristas.get(k);
    }
    public boolean existeArista(String k){
        return aristas.containsKey(k);
    }
    // estado
    public void addEstadoGP(int k, EstadoGP e){
        estados_gp.put(k, e);
    }
    public EstadoGP getEstadoGP(int k){
        return estados_gp.get(k);
    }
    //
    public int idEstadoActual(Point p){
        Iterator <EstadoGP> it = estados_gp.values().iterator();
        EstadoGP aux;
        while(it.hasNext()){
            aux = it.next();
            if(aux.puntoDentro(p.x, p.y)){
                return aux.getPosId();
            }
        }
        return -1;
    }
    
    public Arist_A idAristaActual(Point p){
        Iterator <Arist_A> it = aristas.values().iterator();
        Arist_A aux;
        while(it.hasNext()){
            aux = it.next();
            if(aux.puntoDentroSimbolos(p.x, p.y)){
                return aux;
            }
        }
        return null;
    }
    
}
