/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package op;

import at.Aut;
import at.Est;
import at.Simb;
import at.Trans;
import diag.Arist_A;
import diag.Diag;
import diag.EstadoGP;
import diag.ArAjust_A;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ConversionAFND_AFD {
    private Aut afnd;
    private Aut afd; 
    public ConversionAFND_AFD(Aut afnd)  {
        this.afnd = afnd;
        afd = new Aut();
        afd.alfabeto = afnd.alfabeto;
    }
    
    private boolean hayEsFinalesAFD(ArrayList <String> estados) {
        ArrayList <Integer> esf = afnd.estadosFinales;
        Iterator <String> it = estados.iterator();
        while( it.hasNext() ) { 
            if(esf.contains( Integer.parseInt( it.next() )) ) {
                return true;
            }
        }
        return false;
    }
    
    private int existeStrEstado(String nombre) {
        Iterator <Integer> it = afd.estados.keySet().iterator();
        while(it.hasNext()) {
            int aux = it.next();
            if( afd.getEstado( aux ).getNombre().compareTo(nombre) == 0) {
               return  aux;
            }
        }
        return -1;
    }
    
    private void delta(int estado, int simbolo, Set <String> es) {
       Iterator<String> it = afnd.getEstado(estado).getIdTransiciones().iterator();
       while(it.hasNext()) {
           String aux = it.next();
           Trans t = afnd.getTransicion(aux);
           if(t.existeSimbolo(simbolo) && t.getPosId_P() == estado) {
               es.add( aux.split("-")[1] );
           }
       }
    }
    
    public Set <String> producto(String[] estados, int simbolo) {
        Set <String> es = new HashSet<>();
        int i = 0;
        while(i < estados.length) {
            delta(Integer.parseInt( estados[i] ), simbolo, es);
            i++;
        }
        return es;
    }
    
    public void conversion() {
        int len1 = 0;
        int len2 = 1;
        
        // añadimos el 1er estado del afnd al afd
        afd.addEstado(len1, new Est(len1, afnd.getEstado(len1).getPosId() +""));
        afd.estadoInicial = len1;
        
        while(len1 <= (len2-1)) {
            
            Iterator <Simb> it_alf = afnd.alfabeto.iterator();
            while(it_alf.hasNext()) {
                
                Est estado = afd.getEstado(len1);
                String []des = estado.getNombre().split(",");
                Simb sim = it_alf.next();
                Set <String> es = producto(des, sim.getPos());
                
                if(!es.isEmpty()) {
                    ArrayList <String> lista = new ArrayList<>(es);
                    Collections.sort(lista);
                    Iterator <String> it = lista.iterator();
                    String nn = it.next();
                    //
                    while(it.hasNext()) {
                        nn += "," + it.next();
                    }
                    int est = existeStrEstado(nn);
                    if( est == -1) {
                        est = len2;
                        afd.addEstado(est, new  Est(est, nn));
                        if( hayEsFinalesAFD(lista) ) {
                            afd.estadosFinales.add(est);
                        }
                        System.out.println(est + ": " + nn);
                        len2++;
                    }                    
                    
                    Trans taux = afd.getTransicion(len1+"-"+est);
                    
                    if(taux == null) {
                        afd.addTransicion(len1+"-"+est, new Trans(len1, sim.getPos(), est));
                        afd.getEstado(len1).addIdTransicion(len1+"-"+est);
                        //
                        afd.getEstado(est).addIdTransicion(len1+"-"+est);
                        //
                        System.out.println("T: " + len1 + "," + sim.getPos() + "," + est);
                    } else {
                        System.out.println("T: " + len1 + "," + sim.getPos() + "," + est);
                        taux.addNuevoSimbolo(sim.getPos());
                    }
                }
                
            }
            len1++;
        }
    }
    
    // Crear Diag AFD
    public Diag diagramaAFD(Dimension d) {
        Diag di = new Diag();
        
        int espacio_w = d.width / 4;
        int espacio_h = ( d.height - 500)/ (afd.estados.size()/2 + 1);
        int inicio_w = 100;
        int inicio_h = 100;
        
        Iterator <Integer> it = afd.estados.keySet().iterator();
        Est aux;
        int i = 0;
        while(it.hasNext()) {

            if(i%2==0) {
                inicio_h = (i * espacio_h) + 100;
                inicio_w = 100;
            } else {
                inicio_w = 100 + espacio_w;
            }
            
            aux = afd.getEstado( it.next() );
            EstadoGP eg = new EstadoGP(aux.getPosId(), 
                    new Point(inicio_w, inicio_h), 
                    aux.getPosId()+"");
            di.addEstadoGP(aux.getPosId(), eg);
            //
            i++;
        }
        
        di.getEstadoGP( afd.estadoInicial ).setInicial(true);
        
        Iterator <Integer> it_fs = afd.estadosFinales.iterator();
        while(it_fs.hasNext()) {
            di.getEstadoGP( it_fs.next() ).setFinal(true);
        }
        
        Iterator <Integer> it_gp = di.estados_gp.keySet().iterator();
        while(it_gp.hasNext()) {
            Integer es_id =  it_gp.next();
            aux = afd.getEstado( es_id );
            EstadoGP eg = di.getEstadoGP( es_id );
            
            ArrayList <String> trans =  aux.getIdTransiciones();
            if(!trans.isEmpty()) {
                crearAristas(di, eg, trans);
            }
        }
        return di;
    }
    
    private void crearAristas(Diag di, EstadoGP es, ArrayList <String> trans) {
        Iterator <String> it = trans.iterator();
        while(it.hasNext()) {
            String idTransicion = it.next();
            //
            String []es_vec = idTransicion.split("-");
            //
            int estado_p = Integer.parseInt( es_vec[0] );
            if(es.getPosId() == estado_p) {
                String str_ar = crear_StrArista(idTransicion);
                int estado_l = Integer.parseInt( es_vec[1] );
                
                int tipo = ArAjust_A.LINEA;
                if(estado_l == estado_p){
                    tipo = ArAjust_A.ARCO;
                }else if(di.existeArista( estado_l+"-"+estado_p )) {
                    tipo = ArAjust_A.LINEA_QUEBRADA;
                    di.getArista(estado_l+"-"+estado_p).setNuevoTipo(tipo, di);
                } 
                
                Arist_A ar = new Arist_A(estado_p, str_ar, estado_l, tipo, di);
                //
                di.addArista(idTransicion, ar);
                //
                es.addIdArista(idTransicion);
                di.getEstadoGP(estado_l).addIdArista(idTransicion);
            }
        }
    }
    
    private String crear_StrArista(String idTransiscion) {
        Trans t = afd.getTransicion(idTransiscion);
        
        Iterator<Integer> it = t.getIDSimbolos().iterator();
        String aux = afd.alfabeto.get( it.next() ).getS();
        while(it.hasNext()) {
            aux += "," + afd.alfabeto.get( it.next() ).getS();
        }
        
        return aux;
    }
    
    public Aut getAFD() {
        return afd;
    }
}
